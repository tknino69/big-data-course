import csv
import avro.schema
from avro.datafile import DataFileWriter
from avro.io import DatumWriter
from hdfs import client

cl = client.Client('http://quickstart.cloudera:50070', root='quickstart.cloudera')
cl.download('/user/student/airlines/airlines.dat','airlines_test.dat')


schema = avro.schema.parse(open("avro_schema.avsc").read())


def csv_reader():
    with open('airlines_test.dat', 'r', encoding='utf-8') as f:
        r = csv.reader(f, delimiter=',')
        for line in r:
            yield [l.encode('utf-8') for l in line]

with open('airlines_test.dat', 'r', encoding='utf-8') as f:
    reader = csv_reader()
    with DataFileWriter(open("airlines_test.avro", "wb"), DatumWriter(), schema, codec='deflate') as writer:
        for count, row in enumerate(reader):
            try:
                writer.append({
                                "field1": int(row[0].decode()),
				"field2": row[1].decode(),
                                "field3": row[2].decode(),
                                "field4": row[3].decode(),
                                "field5": row[4].decode(),
                                "field6": row[5].decode(),
                                "field7": row[6].decode(),
                                "field8": row[7].decode()
                                },
                              )
            except IndexError:
                print('Skip')

cl.upload('/user/student/airlines/airlines_test.avro','airlines_test.avro')
print('file uploaded')